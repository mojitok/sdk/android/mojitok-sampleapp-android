package com.platfarm.example.mojitoksdksampleapp

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.CompoundButton
import android.widget.EditText
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.mojitok.mojitokuikit.ui.MJTBaseFragment
import com.mojitok.mojitokuikit.ui.MJTRTSFragment
import com.mojitok.mojitokcore.schema.event.MJTStickerInfo
import com.mojitok.mojitokuikit.ui.loadMojitokSticker
import com.mojitok.mojitokuikit.viewmodel.MJTStickerChoiceViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.mojitok.mojitokcore.Mojitok

class MainActivity : AppCompatActivity() {
    val TAG = MainActivity::class.java.simpleName

    private lateinit var editMessage: EditText

    var mjtBaseFragment: MJTBaseFragment? = null
    private lateinit var stickerClickViewModel: MJTStickerChoiceViewModel

    var mjtRTSFragment: MJTRTSFragment? = null

    var mIsRTSMode = true
    var mUserChange = true

    // 3-1. Define <Choice> Callback function to be used both in MJTBaseFragment and MJTRTSFragment
    // 3-1. MJTBaseFragment와 MJTRTSFragment에서 사용할 <Choice> callback 함수 정의
    fun callbackToSendSticker(mjtStickerInfo: MJTStickerInfo) {
        val url = mjtStickerInfo.url
        loadMojitokSticker(lifecycleScope, iv_selected_sticker, url)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editMessage = findViewById(R.id.edit_message)

        // 2-3. login to a user
        val mojitokApplicationToken = "{YOUR-MOJITOK-APPLICATION-TOKEN}"
        val USER_A = "{YOUR-APP-USER-ID}"
        val USER_B = "{YOUR-APP-USER-ID-ANOTHER}"
        userLogin(mojitokApplicationToken,USER_A)

        // 3-2. Call callback defined 3-1 when <Choice> a sticker in MJTBassFragment and MJTRTSFragment
        // 3-2. MJTBaseFragment와 MJTRTSFragment에서 스티커를 <선택>할 때 3-1에서 정의한 콜백을 실행
        stickerClickViewModel =
            ViewModelProvider(this).get(MJTStickerChoiceViewModel::class.java)
        stickerClickViewModel.mjtStickerInfo.observe(this, Observer {
            callbackToSendSticker(it)
        })

        // set height of MJT UI
        // MJT UI의 height값 지정
        val height = 719
        // 4-1. Create MJTBaseFragment
        // 4-1. MJTBaseFragment 생성
        mjtBaseFragment = MJTBaseFragment.newInstance(height)

        // 4-2. Integrate the MJTBaseFragment to the <developer>'s fragment
        // 4-2. 고객사의 프래그먼트에 MJTBaseFragment 연결
        supportFragmentManager.beginTransaction()
            .replace(R.id.mjt_fragment_container, mjtBaseFragment!!)
            .commit()

        // 5-1. Create MJTRTSFragment
        // 5-1. MJTRTSFragment 생성
        mjtRTSFragment = MJTRTSFragment()

        // 5-2. MJTRTSFragment integratation to <developer>'s fragment
        // 5-2. 고객사의 fragment에 MJTRTSFragment 연결
        supportFragmentManager.beginTransaction()
            .replace(R.id.mjt_rts_fragment_container, mjtRTSFragment!!)
            .commit()

        /* 5-3. Set to update(searchAndUpdate()) by re-searching recommended stickers in
            MJTRTSFragment whenever the entered text is changed
            ...
            MJTRTSFragment.searchAndUpdate(keyword: String)
            ...
        */
        /* 5-3. 입력된 텍스트가 변경될때마다 MJTRTSFragment 에서 추천 스티커를 다시 검색해서
          갱신(searchAndUpdate())하도록 설정
          ...
          MJTRTSFragment.searchAndUpdate(keyword: String)
          ...
        */

        editMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mjtRTSFragment?.searchAndUpdate(s.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        /*
        *  Optional RTS On/Off Swtich
        * */

        val switchCompat: SwitchCompat = findViewById(R.id.switch_rts_mode)
        val switchUser: SwitchCompat = findViewById(R.id.switch_user)

        switchUser.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, _ ->
            var delayTime = 200
            val fm = supportFragmentManager.beginTransaction()
                    .setCustomAnimations(
                            android.R.animator.fade_in,
                            android.R.animator.fade_out
                    )
            compoundButton.postDelayed({
                if (mUserChange) {
                    fm
                            .hide(mjtRTSFragment!!)
                            .commit()

                    userLogin(mojitokApplicationToken,USER_B)
                } else {
                    fm
                            .show(mjtRTSFragment!!)
                            .commit()

                    userLogin(mojitokApplicationToken,USER_A)
                }
                mUserChange = !mUserChange
            }, delayTime.toLong())
        })
        /*
       *  Optional Login UserChange Swtich (John / Mary)
       * */

        switchCompat.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, _ ->
            var delayTime = 200
            val fm = supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    android.R.animator.fade_in,
                    android.R.animator.fade_out
                )
            compoundButton.postDelayed({
                if (mIsRTSMode) {
                    fm
                        .hide(mjtRTSFragment!!)
                        .commit()
                } else {
                    fm
                        .show(mjtRTSFragment!!)
                        .commit()
                }
                mIsRTSMode = !mIsRTSMode
            }, delayTime.toLong())
        })
    }
    // 이전 유저 정보를 모두 초기화 하여 로그인 시 새로운 유저로 접속
    // Initializes all previous user information and connects as a new user when logging in
    fun userLogin(mojitokApplicationToken: String,userId: String){
        CoroutineScope(Dispatchers.IO).launch {
            Mojitok.login(mojitokApplicationToken, userId, { e ->
                if (e == null) {
                    // 2-3-a. on Success
                    // 2-3-a. 성공 시
                } else {
                    // 2-3-b. on Fail: If the e.code value is 4001, then please check the applicationId or  the applicationToken
                    // 2-3-b. 실패 시: e.code 값이 4001이라면 applicationId 또는 Token을 확인해주세요
                    Log.d("MJT", String.format("%d%s", e.code, e.message))
                }
            }, {
                // 2-4. (Optional) Register ApplicationToken Update callback function
                // 2-4. (Optional) ApplicationToken Update 콜백함수 등록
                Log.d("MJT", "ReplaceTokenListener Event")
            })
        }
    }
    fun userLogout(){
        Mojitok.logout()
    }
    override fun attachBaseContext(newBase : Context) {
        val language = "en"
        super.attachBaseContext(LocaleContextWrapper.wrap(newBase, language!!));
    }

}
